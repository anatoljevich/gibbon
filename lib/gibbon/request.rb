module Gibbon
  class Request
    attr_accessor :api_key, :api_endpoint, :timeout, :proxy, :faraday_adapter, :debug, :logger

    DEFAULT_TIMEOUT = 30

    def initialize(opts = {api_key: nil, api_endpoint: nil, timeout: nil, proxy: nil, faraday_adapter: nil, debug: false, logger: nil})
      @path_parts = []
      @api_key = opts[:api_key] || self.class.api_key || ENV['MAILCHIMP_API_KEY']
      @api_key = @api_key.strip if @api_key
      @api_endpoint = opts[:api_endpoint] || self.class.api_endpoint
      @timeout = opts[:timeout] || self.class.timeout || DEFAULT_TIMEOUT
      @proxy = opts[:proxy] || self.class.proxy || ENV['MAILCHIMP_PROXY']
      @faraday_adapter = opts[:faraday_adapter] || Faraday.default_adapter
      @logger = opts[:logger] || self.class.logger || ::Logger.new(STDOUT)
      @debug = opts[:debug]
    end

    def method_missing(method, *args)
      # To support underscores, we replace them with hyphens when calling the API
      @path_parts << method.to_s.gsub("_", "-").downcase
      @path_parts << args if args.length > 0
      @path_parts.flatten!
      self
    end

    def send(*args)
      if args.length == 0
        method_missing(:send, args)
      else
        __send__(*args)
      end
    end

    def path
      @path_parts.join('/')
    end

    def create(opts = {params: nil, headers: nil, body: nil})
      APIRequest.new(builder: self).post(params: opts[:params], headers: opts[:headers], body: opts[:body])
    ensure
      reset
    end

    def update(opts={params: nil, headers: nil, body: nil})
      APIRequest.new(builder: self).patch(params: opts[:params], headers: opts[:headers], body: opts[:body])
    ensure
      reset
    end

    def upsert(opts={params: nil, headers: nil, body: nil})
      APIRequest.new(builder: self).put(params: opts[:params], headers: opts[:headers], body: opts[:body])
    ensure
      reset
    end

    def retrieve(opts={params: nil, headers: nil})
      APIRequest.new(builder: self).get(params: opts[:params], headers: opts[:headers])
    ensure
      reset
    end

    def delete(opts={params: nil, headers: nil})
      APIRequest.new(builder: self).delete(params: opts[:params], headers: opts[:headers])
    ensure
      reset
    end

    protected

    def reset
      @path_parts = []
    end

    class << self
      attr_accessor :api_key, :timeout, :api_endpoint, :proxy, :logger

      def method_missing(sym, *args, &block)
        new({api_key: self.api_key, api_endpoint: self.api_endpoint, timeout: self.timeout, proxy: self.proxy, logger: self.logger}).send(sym, *args, &block)
      end
    end
  end
end
